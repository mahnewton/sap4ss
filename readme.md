
# Secondary Structure Specific Simpler Prediction Models for Protein Backbone Angles (SAP4SS)

This project provides the programs and the files related to SAP4SS that predicts protein backbone angles phi, psi, theta, and tau. 


## Requirements

To run the program, for each protein named **protein_name**, you need the following files

	- protein_name.pssm
	- protein_name.hhm
	- protein_name.out.ss
	- protein_name.out.ss8
	- protein_name.spot1d
	- protein_name.dssp (Optional)
	- protein_name.t (Optional)
 
All of these files are to be in the **inputs** folder.


## Dependencies

The program provided might depend on the following packages and versions.
	
	- numpy 1.20.1
	- keras 2.4.3


## Getting Input Files


### Generating PSSM Files (.pssm):

	- Visit the website "https://ftp.ncbi.nlm.nih.gov/blast/executables/LATEST/"
	- Install the BLAST. 
	- Dowload the nr dataset "UniRef90 sequence database updated in April 2018" from the website "https://www.uniprot.org/help/uniref".
	- Use the following command to extract the .pssm file.
		- psiblast -query protein_name.fasta -db nr -num_iterations 3 -out_ascii_pssm protein_name.pssm



### Generating HHM File (.hhm):
	- Visit the website "https://github.com/soedinglab/hh-suite/wiki?fbclid=IwAR3va97j9DHRMtOfVMu8eGhKXYSBH7GZGIUm5vSvc8nmpV0xG25CaI72pfI"
	- Install the HHsuite and its databases. 
	- Download the uniprot dataset "Uniprot sequence profile database from October 2017" from the website "https://www.uniprot.org/downloads".
	- Use the following command to extract the .hhm file.
		- hhblits -i $protein_name.fasta -ohhm $protein_name.hhm -oa3m $protein_name.a3m -d uniprot



### Generating SSPro outputs (.out.ss8, and .out.ss files):
	- Visit the website "http://download.igb.uci.edu/" and download SCRATCH-1D release 1.2 package.
	- To install SCRATCH-1D, unarchive the downloaded tarball:
		- tar -xvzf SCRATCH-1D_1.2.tar.gz
	- Change directory to the resulting folder:
		- cd SCRATCH-1D_1.2
	- Run the provided installation script:
		- perl install.pl
	- To test the software installation, change directory to the **doc** sub-folder :
		- cd <INSTALL_DIR>/doc
	- Run SCRATCH-1D from the **doc** folder on the provided test dataset. It needs two inputs: the fasta file of a protein and the name of the output file. (If your computer has less than 4 cores, replace 4 by 1 in the command line below.)
		- ../bin/run_SCRATCH-1D_predictors.sh protein_name.fasta protein_name.out 4




### Generating SPOT-1D outputs (.spot1d file):
	- Visit the website "https://sparks-lab.org/server/spot-1d/"
	- Submit the protein on the SPOT-1D server. 




## Optional Input Files

These are needed if MAE values are to be computed along with angle predictions. These files must be in the **inputs** folder. If you just need the predicted angles but not MAE values, then you do not need these files.

### Getting DSSP File:
	- Visit the website "https://swift.cmbi.umcn.nl/gv/dssp/DSSP_3.html"


### Getting T File:
	- This file contains the native value of Theta and Tau, with the following format:
		1st Column (Residue number) 2nd Column (Amino acid ) 3rd Column (Theta native value) 4th Column (Tau native value) 5th column (Unknown)
	- T files, for the proteins from SPOT-1D set are available but for proteins from PDB150 and CAMEO93, we calculated the native values of theta and tau and save in the same format mentioned above. 
	

## Running the Program

- Update the prot_list.txt with the names of the protein files
- Put the protein data for each protein in a separate folder inside the **inputs** folder
- Run the following commands:
	- To predict the angles and calculate MAE values
		Run **bash RUN-SAP4SS-CalMAE.sh** from the **code** folder
	- To predict the angles only, but no MAE calculation:
		Run **bash RunSAP4SS.sh** from the **code** folder
- The outputs are generated in the **outputs** folder: .phi.sap4ss, .psi.sap4ss, .theta.sap4ss, and .tau.sap4ss  files for each protein.





