
import sys
import numpy as np
import SAP4SS_Functions as sap4ss

from keras.models import model_from_json







"""
Variable Assignment
"""
AngleName = sys.argv[1]
ProteinName = sys.argv[2]
SwitchCalMAE = sys.argv[3]		

AngleIndex = sap4ss.DetermineAngleIndex(AngleName)
SettingNameC = sap4ss.DetermineSAP4SSSettingName(AngleName, 'C')
SettingNameE = sap4ss.DetermineSAP4SSSettingName(AngleName, 'E')
SettingNameH = sap4ss.DetermineSAP4SSSettingName(AngleName, 'H')
WinSizeC, NormVersionC, LenFeaturesC, LenInputNNC, LenOut = sap4ss.SAP4SSParameters(SettingNameC)
WinSizeE, NormVersionE, LenFeaturesE, LenInputNNE, LenOut = sap4ss.SAP4SSParameters(SettingNameE)
WinSizeH, NormVersionH, LenFeaturesH, LenInputNNH, LenOut = sap4ss.SAP4SSParameters(SettingNameH)







"""
Directories
"""
ProteinsFolder = "../inputs/proteins/"
BuffFolder = "../data/"
NNFileC = BuffFolder + "NN_SAP4SS_"+ SettingNameC + ".json"
NNFileE = BuffFolder + "NN_SAP4SS_"+ SettingNameE + ".json"
NNFileH = BuffFolder + "NN_SAP4SS_"+ SettingNameH + ".json"
WFileC = BuffFolder + "W_SAP4SS_" + SettingNameC + ".hdf5"
WFileE = BuffFolder + "W_SAP4SS_" + SettingNameE + ".hdf5"
WFileH = BuffFolder + "W_SAP4SS_" + SettingNameH + ".hdf5"
MinMaxFileC = BuffFolder + "MinMeanMaxSTD_SAP4SS_" + SettingNameC + ".txt"
MinMaxFileE = BuffFolder + "MinMeanMaxSTD_SAP4SS_" + SettingNameE + ".txt"
MinMaxFileH = BuffFolder + "MinMeanMaxSTD_SAP4SS_" + SettingNameH + ".txt"







"""
Reading Data
"""
Proteins = [ProteinName] 
MinMeanC, MaxSTDC = sap4ss.ExtractSAPMinMeanMaxStd(MinMaxFileC)
MinMeanE, MaxSTDE = sap4ss.ExtractSAPMinMeanMaxStd(MinMaxFileE)
MinMeanH, MaxSTDH = sap4ss.ExtractSAPMinMeanMaxStd(MinMaxFileH)







"""
Preprocessing
"""
AllProteinsFeaturesC, AllNativeAngle, AllSS3Pred = sap4ss.SAP4SSPreprocessing(ProteinsFolder, Proteins[:], LenFeaturesC, LenInputNNC, LenOut, WinSizeC, NormVersionC, MinMeanC, MaxSTDC, SwitchCalMAE, 1)
AllProteinsFeaturesE, AllNativeAngle, AllSS3Pred = sap4ss.SAP4SSPreprocessing(ProteinsFolder, Proteins[:], LenFeaturesE, LenInputNNE, LenOut, WinSizeE, NormVersionE, MinMeanE, MaxSTDE, SwitchCalMAE, 1)
AllProteinsFeaturesH, AllNativeAngle, AllSS3Pred = sap4ss.SAP4SSPreprocessing(ProteinsFolder, Proteins[:], LenFeaturesH, LenInputNNH, LenOut, WinSizeH, NormVersionH, MinMeanH, MaxSTDH, SwitchCalMAE, 1)







"""
Testing
"""
json_file = open(NNFileC, 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights(WFileC)
AllPredTestOutputC = loaded_model.predict(AllProteinsFeaturesC)


json_file = open(NNFileE, 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights(WFileE)
AllPredTestOutputE = loaded_model.predict(AllProteinsFeaturesE)


json_file = open(NNFileH, 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights(WFileH)
AllPredTestOutputH = loaded_model.predict(AllProteinsFeaturesH)







"""
Filtering and Merging
"""
MergedSSPredOutputs = sap4ss.MergeProteinPredictedSSParts(AllPredTestOutputC, AllPredTestOutputE, AllPredTestOutputH, AllSS3Pred)







"""
Shifting the predictions to the [-180, 180] range.
"""
ShiftedMergedSSPredOutputs = sap4ss.ShiftingAngle(MergedSSPredOutputs)







"""
Printing SAP4SS Outputs:
"""

print("# ====================================================")
print("# ResidueNumber", "Predicted" + AngleName)
print("# ----------------------------------------------------")

for i in range(ShiftedMergedSSPredOutputs.shape[0]):
	print(i, "%.2f" % ShiftedMergedSSPredOutputs[i, AngleIndex])







"""
calculating, Printing MAE
"""
if SwitchCalMAE == 'MAE-ON':
	AngleMAE = sap4ss.CalMAE4SpecificAngle(AllNativeAngle[:, AngleIndex:AngleIndex + 1], ShiftedMergedSSPredOutputs[:, AngleIndex:AngleIndex + 1])

	print("# ====================================================")
	print("# Angle Name		MAE")
	print("# ----------------------------------------------------")
	print("# ", AngleName, "		%.2f" % AngleMAE)
	print("# ====================================================")






















































