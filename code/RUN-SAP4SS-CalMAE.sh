#!/bin/bash

# run this program from the code folder

input_protein_list=../inputs/prot_list.txt


while IFS= read -r line
do
	python ./SAP4SS.py Phi ${line} MAE-ON > ../outputs/${line}.phi.sap4ss
	python ./SAP4SS.py Psi ${line} MAE-ON > ../outputs/${line}.psi.sap4ss
	python ./SAP4SS.py Theta ${line} MAE-ON > ../outputs/${line}.theta.sap4ss
	python ./SAP4SS.py Tau ${line} MAE-ON > ../outputs/${line}.tau.sap4ss

done < "${input_protein_list}"

