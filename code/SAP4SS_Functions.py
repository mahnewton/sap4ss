
"""
All necessary functions for SAP4SS.
"""

import sys
import math
import numpy as np







LenSSHotvect=8
AngleMaskValue = 360







PhyscDict = {'A': [-0.350, -0.680, -0.677, -0.171, -0.170, 0.900, -0.476], 'C': [-0.140, -0.329, -0.359, 0.508, -0.114, -0.652, 0.476], 'D': [-0.213, -0.417, -0.281, -0.767, -0.900, -0.155, -0.635], 'E': [-0.230, -0.241, -0.058, -0.696, -0.868, 0.900, -0.582], 'F': [ 0.363, 0.373, 0.412, 0.646, -0.272, 0.155, 0.318], 'G': [-0.900, -0.900, -0.900, -0.342, -0.179, -0.900, -0.900], 'H': [ 0.384, 0.110, 0.138, -0.271, 0.195, -0.031, -0.106], 'I': [ 0.900, -0.066, -0.009, 0.652, -0.186, 0.155, 0.688], 'K': [-0.088, 0.066, 0.163, -0.889, 0.727, 0.279, -0.265], 'L': [ 0.213, -0.066, -0.009, 0.596, -0.186, 0.714, -0.053], 'M': [ 0.110, 0.066, 0.087, 0.337, -0.262, 0.652, -0.001], 'N': [-0.213, -0.329, -0.243, -0.674, -0.075, -0.403, -0.529], 'P': [ 0.247, -0.900, -0.294, 0.055, -0.010, -0.900, 0.106], 'Q': [-0.230, -0.110, -0.020, -0.464, -0.276, 0.528, -0.371], 'R': [ 0.105, 0.373, 0.466, -0.900, 0.900, 0.528, -0.371], 'S': [-0.337, -0.637, -0.544, -0.364, -0.265, -0.466, -0.212], 'T': [ 0.402, -0.417, -0.321, -0.199, -0.288, -0.403, 0.212], 'V': [ 0.677, -0.285, -0.232, 0.331, -0.191, -0.031, 0.900], 'W': [ 0.479, 0.900, 0.900, 0.900, -0.209, 0.279, 0.529], 'Y': [ 0.363, 0.417, 0.541, 0.188, -0.274, -0.155, 0.476]}






SSHotvectDict = {'B' : [1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0], 'C' : [0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0], 'E' : [0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0], 'G' : [0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0], 'H' : [0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0], 'I' : [0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0], 'S' : [0.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0], 'T' : [0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0]}









def DetermineSAP4SSSettingName(AngleName, SSMask):

	if AngleName !="Phi" and AngleName != "Psi" and AngleName !="Theta" and AngleName !="Tau":
		raise Exception ('Angle name must be these values: Phi, Psi, Theta, and Tau')
	if SSMask !="C" and SSMask != "E" and SSMask !="H":
		raise Exception ('SS Mask name must be these values: C, E, and H')

	if SSMask == "C":
		if AngleName =="Phi" or AngleName =="Theta":
			SettingName = "C1"
		else:
			SettingName = "C2"

	elif SSMask == "E":
		if AngleName =="Psi":
			SettingName = "E2"
		else:
			SettingName = "E1"
	else:
		SettingName = "H"
	return SettingName







def SAP4SSParameters(SettingName):

	LenFeatures = 8 + 20 + 7 + 1 + 20
	WinSize, LenOut = 4, 4
	NormVersion = "Range" 

	if SettingName == "C1": 
		WinSize = 2
	if SettingName == "E2":
		NormVersion = "Zscore" 
	LenInputNN = ( 2 * WinSize + 1) * LenFeatures

	return WinSize, NormVersion, LenFeatures, LenInputNN, LenOut







def ExtractSAPMinMeanMaxStd(MinMeanMaxStdFile):

	with open(MinMeanMaxStdFile) as p:
		Lines = p.readlines()
		p.close()
		MinMean, MaxSTD = [], []
		for Line in Lines:
			SplitLine = Line.strip().split()
			if (len(SplitLine) != 0 )and (SplitLine[0] != '#'):
				MinMean.append(float(SplitLine[0]))
				MaxSTD.append(float(SplitLine[1]))
		MinMean=np.array(MinMean).reshape(1, -1)
		MaxSTD=np.array(MaxSTD).reshape(1, -1)
	return MinMean, MaxSTD







def ExtractSSPredicted(SSProFile):

	with open(SSProFile) as p:
		Lines = p.readlines()
		p.close()
	SSPred = Lines[1].strip()
	return SSPred








def ExtractAAFasta(FastaFile):
	with open(FastaFile) as p:
		Lines = p.readlines()
		p.close()
	AA = Lines[1].strip()
	return AA








def ExtractPSSMMtx(PSSMFile):

	PSSMMtx = np.zeros((0,20))
	with open(PSSMFile) as p:
		Lines = p.readlines()
		p.close()
	for Line in Lines:
		SplitLine = Line.strip().split()
		if (len(SplitLine) == 44) and (SplitLine[0] != '#'):			
			PSSMRow = np.array([-float(i) for i in SplitLine[2:22]]).reshape(1, -1)
			PSSMMtx = np.concatenate((PSSMMtx, PSSMRow), axis=0)

	return PSSMMtx








def ExtractHHM(HHMFile):

	HMM = open(HHMFile, "r")
	Length, Got_Len, HMMMtx = -1, False, []

	while True:
		Line_HMM = HMM.readline()
		if Line_HMM.startswith("LENG") and Got_Len==False:
			Line_Content = Line_HMM.strip().split()
			Length = int(Line_Content[1])
			Got_Len = True
		if Line_HMM.startswith("#"):
			break

	HMM.readline(); HMM.readline(); HMM.readline(); HMM.readline();
	for i in range(0, Length):
		HMM_List = []
		Residue_Line = HMM.readline()
		Residue_Split_Line = Residue_Line.strip().split()
		for j in range(2 , 22):
			HMM_Each_AA = 0
			if Residue_Split_Line[j] == "*":
				HMM_Each_AA = 0
			else:
				HMM_Each_AA = math.pow(2, ((-1 *float(Residue_Split_Line[j]))/1000))
 
			HMM_List.append(HMM_Each_AA )
		HMM.readline()
		HMM.readline()
		HMMMtx.append(HMM_List)
	HMMMtx = np.array(HMMMtx)
	return HMMMtx







def ExtractASAPrediction(Spot1dFile):

	with open(Spot1dFile) as P:
		Lines = P.readlines()
		ASAPred = []		
		for Line in Lines:
			Line = Line.strip()
			SplitLine = Line.split()
			if (len(SplitLine) != 0 )and (SplitLine[0] != '#'):
				ASAPred.append(float(SplitLine[4]))

		P.close()
	ASAPred = np.array(ASAPred).reshape(-1, 1)
	return ASAPred







def ConvertSS2Hotvect(SSPred):

	SS8HotMtx = np.zeros((0,LenSSHotvect))
	for i in range (len(SSPred)):
		SS8HotRow = np.array(SSHotvectDict[SSPred[i]]).reshape(1,LenSSHotvect)
		SS8HotMtx = np.concatenate((SS8HotMtx, SS8HotRow), axis=0)
	return SS8HotMtx







def ExtractPhysc(AA):

	PhyscMtx = np.zeros((0,7))
	for i in range(len(AA)):
		PhyscRow = np.array(PhyscDict[AA[i]]).reshape(1, -1)
		PhyscMtx = np.concatenate((PhyscMtx, PhyscRow), axis=0)
	return PhyscMtx







def SlidingWindow(ProteinName, WinSize, InputMtx, LenFeatures):

	LenNNInput= (2 * WinSize + 1) * LenFeatures
	LenProtein = InputMtx.shape[0]
	WinPartProtein = np.zeros((0,LenNNInput))
	WinWholeProtein = np.zeros((0,LenNNInput))

	for Core in range (0, LenProtein, 1):
		LeftWin = Core - WinSize
		RightWin = Core + WinSize
		if RightWin >= LenProtein:
			RightWin = RightWin-LenProtein
			WinPartProtein = np.concatenate((InputMtx[LeftWin:, :].ravel(), InputMtx[:RightWin+1, :].ravel()), axis=0)
			WinPartProtein = WinPartProtein.reshape (1, -1)
		elif LeftWin < 0:
			WinPartProtein = np.concatenate((InputMtx[LeftWin:, :].ravel(), InputMtx[:RightWin+1, :].ravel()), axis=0)
			WinPartProtein = WinPartProtein.reshape (1, -1)
		else:
			WinPartProtein = InputMtx[LeftWin:RightWin+1, :] 
			WinPartProtein = WinPartProtein.ravel()
			WinPartProtein = WinPartProtein.reshape (1, WinPartProtein.shape[0])
		if WinWholeProtein.shape[1] != WinPartProtein.shape[1]:
			print(WinWholeProtein.shape, WinPartProtein.shape)
			print(ProteinName)
			print(LenProtein, LeftWin, RightWin, Core)
		WinWholeProtein =  np.concatenate((WinWholeProtein, WinPartProtein ), axis=0)
	return WinWholeProtein







def Normalization(InputMtx, NormVersion, Reuse, MinMean, MaxSTD):

	if (NormVersion == "Range"):
		if (Reuse == 0):
			MinMean = np.min(InputMtx, axis = 0).reshape(1, -1)
			MaxSTD = np.max(InputMtx, axis = 0).reshape(1, -1)
		for i in range (0, InputMtx.shape[1]):
			InputMtx[:,i] = (InputMtx[:, i] - float(MinMean[0,i]))/((float(MaxSTD[0,i])-float(MinMean[0,i])))
	elif(NormVersion == "Zscore"):
		if(Reuse ==0):
			MinMean = np.mean(InputMtx, axis = 0).reshape(1, -1)
			MaxSTD = np.std(InputMtx, axis = 0).reshape(1, -1)
		for i in range (0, InputMtx.shape[1]):
			InputMtx[:,i] = (InputMtx[:, i] - float(MinMean[0,i]))/(float(MaxSTD[0,i]))
	return InputMtx, MinMean, MaxSTD








def ExtractPhiPsi(DSSPFile):

	with open(DSSPFile) as p:
		Lines = p.readlines()
		p.close()
	Phi = [float(i) for i in Lines[3].strip().split(' ')]
	Psi = [float(i) for i in Lines[4].strip().split(' ')]
	Phi = ConvrtList2NPArray(Phi)
	Psi = ConvrtList2NPArray(Psi)
	return  Phi, Psi







def ExtractThetaTau(TFile):

	with open(TFile) as p:
		Lines = p.readlines()
		Theta, Tau = [], []
		for Line in Lines:		
			SplitLine = Line.strip().split()
			if (len(SplitLine) != 0 and len(SplitLine[0]) != 0):
				if (SplitLine[0][0] != '#'):				
					Theta.append(float(SplitLine[2]))
					Tau.append(float(SplitLine[3]))
		p.close()
	Theta = ConvrtList2NPArray(Theta)
	Tau = ConvrtList2NPArray(Tau)
	return Theta, Tau







def ConvrtList2NPArray(InputList):
	return np.array(InputList).reshape(-1, 1)








def SAP4SSPreprocessing(ProteinFolder, Proteins, LenFeatures, LenInputNN, LenOut, WinSize, NormVersion, MinMean, MaxSTD, SwitchCalMAE, Reuse):

	AllProteinsFeatures = np.zeros((0, LenInputNN))
	AllProteinsAngles = np.zeros((0, LenOut))
	AllSS3Pred = ""
	
	for Protein in Proteins:

		FastaFile = ProteinFolder +  Protein + "/" + Protein + ".fasta"
		PSSMFile = ProteinFolder +  Protein + "/" + Protein + ".pssm"
		HHMFile = ProteinFolder +  Protein + "/" + Protein + ".hhm"
		SS8ProFile = ProteinFolder + Protein + "/" + Protein + ".out.ss8"
		SS3ProFile = ProteinFolder + Protein + "/" + Protein + ".out.ss"
		Spot1dFile = ProteinFolder + Protein + "/" + Protein + ".spot1d"

		AA = ExtractAAFasta(FastaFile)
		SS8Pred = ExtractSSPredicted(SS8ProFile)
		SS3Pred = ExtractSSPredicted(SS3ProFile)
		PSSMMtx = ExtractPSSMMtx(PSSMFile)
		HHMMtx = ExtractHHM(HHMFile)
		ASAMtx = ExtractASAPrediction(Spot1dFile)
		SS8Hotvect = ConvertSS2Hotvect(SS8Pred)
		PhyscMtx = ExtractPhysc(AA)


		if SwitchCalMAE == 'MAE-ON':
			TFile = ProteinFolder + Protein + "/" + Protein + ".t"
			DSSPFile = ProteinFolder + Protein + "/" + Protein + ".dssp"		
			Phi, Psi = ExtractPhiPsi(DSSPFile)
			Theta, Tau = ExtractThetaTau(TFile)
			ProteinAngles = np.concatenate((Phi, Psi, Theta, Tau), axis = 1)
			AllProteinsAngles = np.concatenate((AllProteinsAngles, ProteinAngles), axis = 0)

		ProteinFeatures = np.concatenate((SS8Hotvect, PSSMMtx, PhyscMtx, ASAMtx, HHMMtx), axis = 1)
		ProteinWindowedFeatures = SlidingWindow(Protein, WinSize, ProteinFeatures, LenFeatures)
		AllProteinsFeatures = np.concatenate((AllProteinsFeatures, ProteinWindowedFeatures), axis = 0)
		AllSS3Pred += SS3Pred

	AllProteinsFeatures, MinMean, MaxSTD = Normalization(AllProteinsFeatures, NormVersion, Reuse, MinMean, MaxSTD)

	return AllProteinsFeatures, AllProteinsAngles, AllSS3Pred







def MergeProteinPredictedSSParts(AllPredC, AllPredE, AllPredH, SSSeq):

	MergePredBasedSS = np.zeros((0, AllPredC.shape[1]))
	for i in range(0, len(SSSeq)):
		if(SSSeq[i] == 'C'):
			MergePredBasedSS = np.concatenate((MergePredBasedSS, AllPredC[i, :].reshape(1, -1)), axis=0)
		elif(SSSeq[i] == 'E'):
			MergePredBasedSS = np.concatenate((MergePredBasedSS, AllPredE[i, :].reshape(1, -1)), axis=0)
		elif(SSSeq[i] == 'H'):
			MergePredBasedSS = np.concatenate((MergePredBasedSS, AllPredH[i, :].reshape(1, -1)), axis=0)
		else:
			raise Exception ('Secondary structure must have three values while SS for ith resideue is:', AASSSeq[i])
	return MergePredBasedSS







def DefineMask360Degree(NativeAngles):

	Mask = np.zeros(NativeAngles.shape)
	for i in range (0, NativeAngles.shape[0]):
		for j in range (0, NativeAngles.shape[1]):
			if (NativeAngles[i,j] != AngleMaskValue):
				Mask[i,j] = 1.0
	return Mask







def MaskedMatrix(InputMtx, Mask):
	MaskedInputMtx = InputMtx * Mask
	return MaskedInputMtx









def ShiftingAngle(InputMtx):

	ShiftedInputMtx = InputMtx
	for i in range(0, InputMtx.shape[0]):
		for j in range(0, InputMtx.shape[1]):
			if (InputMtx[i, j] > 180):
				ShiftedInputMtx[i, j] = InputMtx[i, j] - 360
			elif (InputMtx[i, j] <= -180):
				ShiftedInputMtx[i, j] = InputMtx[i, j] + 360
	return ShiftedInputMtx








def CalMAE4SpecificAngle(NativeAngles, PredAngles):

	Mask = DefineMask360Degree(NativeAngles)
	MaskedNative = MaskedMatrix(NativeAngles, Mask)
	MaskedPred = MaskedMatrix(PredAngles, Mask)
	Err = abs(MaskedPred - MaskedNative)
	P_Plus_Err = abs(MaskedPred + 360 - MaskedNative)
	P_Minus_Err = abs(MaskedPred - 360 - MaskedNative)

	for i in range (0, Err.shape[0]):
		for j in range (0, Err.shape[1]):
			E = min(Err[i, j], P_Plus_Err[i, j], P_Minus_Err[i, j])
			if (Err[i,j] >= 180):
				Err[i,j] = abs(360 - Err[i,j])
				
				if (abs(E - Err[i, j]) > 0.01):
					print(E, Err[i, j])

	Err = abs(Err)
	MAE = np.sum(Err)/np.sum(Mask)

	return MAE





def DetermineAngleIndex(AngleName):
	if AngleName == "Phi":
		AngleIndex = 0
	elif AngleName == "Psi":
		AngleIndex = 1
	elif AngleName == "Theta":
		AngleIndex = 2
	elif AngleName == "Tau":
		AngleIndex = 3
	else:
		raise Exception ('Angle name must be these values: Phi, Psi, Theta, and Tau')
	return AngleIndex





		





